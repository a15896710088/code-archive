import javax.swing.*;
import java.awt.*;

public class class1 {
    public static void main(String[] args){
        JFrame top=new JFrame("常用组件随堂练习");
        top.setVisible(true);
        top.setSize(500,500);
        JLabel label=new JLabel("输入框：");
        JTextField textField=new JTextField(10);
        JButton button=new JButton("确定");
        JCheckBox checkBox1=new JCheckBox("喜欢音乐");
        JCheckBox checkBox2=new JCheckBox("喜欢篮球");
        JCheckBox checkBox3=new JCheckBox("喜欢网球");
        JRadioButton radioButton1=new JRadioButton("数学");
        JRadioButton radioButton2=new JRadioButton("Java");
        JComboBox<String> comboBox=new JComboBox<String>();
        comboBox.addItem("音乐天地");
        comboBox.addItem("武术天地");
        JTextArea textArea=new JTextArea("欢迎学习Swing组件！");
        JPanel panel1=new JPanel();
        JPanel panel2=new JPanel();
        JPanel panel3=new JPanel();
        panel1.setLayout(new FlowLayout());
        panel2.setLayout(new FlowLayout());
        panel3.setLayout(new FlowLayout());
        panel1.add(label);
        panel1.add(textField);
        panel1.add(button);
        panel2.add(checkBox1);
        panel2.add(checkBox2);
        panel2.add(checkBox3);
        panel3.add(radioButton1);
        panel3.add(radioButton2);
        panel3.add(comboBox);
        Box boxV=Box.createVerticalBox();
        boxV.add(panel1);
        boxV.add(panel2);
        boxV.add(panel3);
        Box baseBox=Box.createVerticalBox();
        baseBox.add(boxV);
        baseBox.add(textArea);
        top.add(baseBox);
    }
}
