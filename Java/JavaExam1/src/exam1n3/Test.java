package exam1n3;

public class Test {
    public static void main(String[] args){
        Account user=new Account("江山",1122,10000);
        user.changeInterestRate(0.015);
        user.deposit(500);
        user.deposit(1000);
        user.deposit(5000);
        user.withDraw(100);
        user.withDraw(2000);
        user.withDraw(3000);
        user.printLogs();
    }
}
