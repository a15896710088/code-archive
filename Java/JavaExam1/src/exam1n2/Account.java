package exam1n2;

import java.util.Date;

public class Account {
    Account(String account,double balance){
        this.account=account;
        this.balance=balance;
    }
    private String account="NULL";
    protected double balance=0;
    private static double annualInterestRate=0;
    private Date regDate=new Date();
    String getAccount(){
        return account;
    }
    void changeAccount(String id){
        this.account=id;
    }
    double getBalance(){
        return balance;
    }
    void changeBalance(double balance){
        this.balance=balance;
    }
    double getInterestRate(){
        return annualInterestRate;
    }
    public void changeInterestRate(double annualInterestRate){
        this.annualInterestRate =annualInterestRate;
    }
    Date getRegDate(){
        return regDate;
    }
    double getMonthInterestRate(){
        return annualInterestRate/12;
    }
    double withDraw(double money){
        if(balance<money){
            System.out.println("余额不足！");
            return 0;
        }
        else {
            balance -= money;
            return balance;
        }
    }
    double deposit(double money){
        balance+=money;
        return balance;
    }
    private String accountType(){
        return "default Account";
    }
    public String toString() {
        return "{\nAccountType=\t"+accountType()+"\nBalance\t:"+balance+"\nMonthInterestRate:\t"+getMonthInterestRate()+"\nRegDate:\t"+regDate+"\n}";
    }
}
