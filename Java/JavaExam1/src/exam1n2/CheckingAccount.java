package exam1n2;

public class CheckingAccount extends Account {
    CheckingAccount(String account, double balance, double overdraftLimit) {
        super(account, balance);
        this.overdraftLimit = overdraftLimit;
    }

    double overdraftLimit = 0;

    String accountType() {
        return "Checking Account";
    }

    double withDraw(double money) {
        if (money - balance > overdraftLimit) {
            System.out.println("透支余额不足！");
            return 0;
        } else {
            balance -= money;
            return balance;
        }
    }
    public String toString() {
        return "{\nAccountType=\t" + accountType() + "\nBalance\t:" + balance + "\nMonthInterestRate:\t" + getMonthInterestRate() + "\nRegDate:\t" + getRegDate() + "\nOverDraftLimit:"+overdraftLimit+"\n}";
    }
}
