package exam1n2;

public class SavingAccount extends Account{
    SavingAccount(String account,double balance){
        super(account,balance);
    }
    String accountType(){
        return "SavingAccount";
    }
    public String toString() {
        return "{\nAccountType=\t" + accountType() + "\nBalance\t:" + balance + "\nMonthInterestRate:\t" + getMonthInterestRate() + "\nRegDate:\t" + getRegDate() + "\nOverDraftLimit:"+"0"+"\n}";
    }
}
