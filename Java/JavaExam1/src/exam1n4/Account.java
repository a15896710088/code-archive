package exam1n4;

import exam1n4.Transaction;

import java.util.ArrayList;
import java.util.Date;

public class Account {
    Account(String account, int ID, double balance){
        this.account=account;
        this.id=ID;
        this.balance=balance;
    }
    private int id=0;
    int getId(){
        return id;
    }
    void changeId(int id){
        this.id=id;
    }
    private String account="NULL";
    protected double balance=0;
    private static double annualInterestRate=0;
    private Date regDate=new Date();
    String getAccount(){
        return account;
    }
    void changeAccount(String id){
        this.account=id;
    }
    double getBalance(){
        return balance;
    }
    void changeBalance(double balance){
        this.balance=balance;
    }
    double getInterestRate(){
        return annualInterestRate;
    }
    public void changeInterestRate(double annualInterestRate){
        this.annualInterestRate =annualInterestRate;
    }
    Date getRegDate(){
        return regDate;
    }
    double getMonthInterestRate(){
        return annualInterestRate/12;
    }
    ArrayList<Transaction> transactions=new ArrayList<Transaction>();
    void printLogs(){
        System.out.println("所有者："+account+"\n年利率："+annualInterestRate+"\n余额："+balance);
        System.out.println("所有交易：\n时间：\t\t\t\t\t\t\t类型：\t交易量：\t余额：\t交易描述：\t");
        for(int i=0;i<transactions.size();i++){
            System.out.print(transactions.get(i).getDate()+"\t");
            System.out.println(transactions.get(i).getType()+"\t\t"+transactions.get(i).getAmount()+"\t"+transactions.get(i).getBalance()+"\t"+transactions.get(i).getDescription());
        }
    }
    double withDraw(double money){
        if(balance<money){
            System.out.println("余额不足！");
            return 0;
        }
        else {
            balance -= money;
            transactions.add(new Transaction('W',money,balance,"normalWithDraw"));
            return balance;
        }
    }
    double deposit(double money){
        balance+=money;
        transactions.add(new Transaction('D',money,balance,"normalDeposit"));
        return balance;
    }
}
