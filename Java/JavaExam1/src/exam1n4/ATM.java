package exam1n4;

import java.util.ArrayList;
import java.util.Scanner;

public class ATM {
    public static void main(String[] args){
        ArrayList<Account> accounts=new ArrayList<Account>();
        for(int i=0;i<=9;i++){
            accounts.add(new Account("userATM",i,100));
        }
        while(true) {
            System.out.println("enter an id:");
            Scanner in = new Scanner(System.in);
            int account = 100;
            boolean mark = false;
            while (!mark) {
                account = in.nextInt();
                if (account >= 10) {
                    System.out.println("Input invalid!");
                }
                else{
                    mark=true;
                }
            }
            int choice;
            System.out.println("Main menu \n1: check balance \n2: withdraw \n3: deposit \n4: exit \nEnter a choice:");
            choice = in.nextInt();
            switch (choice) {
                case 1:{
                    System.out.println("The balance is "+accounts.get(account).getBalance());
                    break;
                }
                case 2:{
                    double amount=0;
                    System.out.println("Enter an amount to withdraw: ");
                    amount=in.nextDouble();
                    accounts.get(account).withDraw(amount);
                    break;
                }
                case 3:{
                    System.out.println("Enter an amount to deposit: ");
                    double amount=0;
                    amount=in.nextDouble();
                    accounts.get(account).deposit(amount);
                    break;
                }
                case 4:
                {
                    break;
                }
            }
        }
    }

}
