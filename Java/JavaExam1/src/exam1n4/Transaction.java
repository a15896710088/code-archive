package exam1n4;

import java.util.Date;

public class Transaction {
    private Date date=new Date();
    private char type;
    private double amount;
    private double balance;
    private String description;
    public Date getDate(){
        return date;
    }
    void setType(char type){
        this.type=type;
    }
    public char getType(){
        return type;
    }
    void setAmount(double amount){
        this.amount=amount;
    }
    public double getAmount(){
        return amount;
    }
    void setBalance(double balance){
        this.balance=balance;
    }
    public double getBalance(){
        return balance;
    }
    void setDescription(String description){
        this.description=description;
    }
    public String getDescription(){
        return description;
    }
    public Transaction(char type, double amount, double balance, String description){
        this.type=type;
        this.amount=amount;
        this.balance=balance;
        this.description=description;
    }
}
