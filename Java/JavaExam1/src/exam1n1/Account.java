package exam1n1;
import java.util.Date;

public class Account {
    Account(){
        dateCreate=new Date();
    };//5
    Account(int id,int balance){
        this.id=id;
        this.balance=balance;
        dateCreate=new Date();
    }//6
    //构造函数
    private int id=0;//1
    private double balance=0;//2
    private static double annualInterestRate=0;//3
    private Date dateCreate;//4
    int getId(){
        return id;
    }
    void changeId(int id){
        this.id=id;
    }
    double getBalance(){
        return balance;
    }
    void changeBalance(double balance){
        this.balance=balance;
    }
    double getInterestRate(){
        return annualInterestRate;
    }
    public void changeInterestRate(double annualInterestRate){
        this.annualInterestRate=annualInterestRate;
    }
    Date getDateCreate(){
        return dateCreate;
    }
    double getMonthInterestRate(){
        return annualInterestRate/12;
    }
    double withDraw(double money){
        if(balance<money){
            System.out.println("余额不足！");
            return 0;
        }
        else {
            balance -= money;
            return balance;
        }
    }
    double deposit(double money){
        balance+=money;
        return balance;
    }
}
