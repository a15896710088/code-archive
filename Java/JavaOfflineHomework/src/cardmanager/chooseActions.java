package cardmanager;

import javax.swing.*;
import javax.swing.border.Border;

public class chooseActions extends JPanel {

    chooseActions(){
        init();
        setVisible(true);
    }
    void init(){
        Border title=BorderFactory.createTitledBorder("选择动作");
        setBorder(title);
        JRadioButton button1=new JRadioButton("查看已有名片");
        JRadioButton button2=new JRadioButton("添加新名片");
        ButtonGroup choice=new ButtonGroup();
        add(button1);
        add(button2);
        choice.add(button1);
        choice.add(button2);
        buttonListener listener=new buttonListener();
        button1.addActionListener(listener);
        button2.addActionListener(listener);
    }
}
