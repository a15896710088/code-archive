//
// Created by misaka13092 on 23-10-11.
//
#include <iostream>
#include <string>
#include <utility>
#include <fstream>
using namespace std;
struct classInf{
    int NO;
    string name;
    string faculty;
    string major;
    int clas;
    int sex;
    int age;
};
class orderList{
    int size;
    classInf* data;
    classInf empty;
public:
    explicit orderList(fstream &in1){
        string temp;
        int count=0;
        while(getline(in1,temp)){
            count++;
        }
        data=new classInf[count];
        in1.seekg(0,ios::beg);
        for(int i=0;i<count;i++&&in1.good()){
            in1>>data[i].NO>>data[i].name>>data[i].faculty>>data[i].major>>data[i].clas>>data[i].sex>>data[i].age;
        }
        size=count;
    }
    explicit orderList(int size1){
        data=new classInf[size1];
        size=size1;
    }
    orderList(){
        data=new classInf[100];//默认构造大小为100的数组
        size=100;
    }
    //以上为创建环节，使用C++的构造函数
    ~orderList(){
        free(data);
    };
    //析构函数，在函数结束运行时清理内存
    int inputData(classInf datum){
        for(int i=0;i<size;i++){
            if(data[i].NO==0){
                data[i]=datum;
                return 1;
            }
        }
    }//输入
    classInf getData(int destination){
        if(destination>=size){
            cout<<"List Overflow!"<<endl;
            return empty;
        }
        else{
            return data[destination];
        }
    }//取值
    int searchData(classInf datum){
        int result;
        int res=0;
        for(int i=0;i<size;i++){
            if(data[i].NO==datum.NO){
                i=result;
                res=1;
                return result;
                break;
            }
        }
        if(res==0){
            cout<<"No matches!"<<endl;
            return 0;
        }
    }//查找
    int deleteData(int destination){
        if(destination>=size){
            cout<<"List Overflow!"<<endl;
            return 0;
        }
        else{
            for(int i=destination+1;i<size;i++){
                data[i-1]=data[i];
            }
            data[size-1]=empty;
            return 1;
        }
    }//删除
    void outputData(){
        cout<<"学号\t姓名\t学院\t专业\t班级\t性别\t年龄\t";
        for(int i=0;i<size;i++){
            cout<<data[i].NO<<"\t"<<data[i].name<<"\t"<<data[i].faculty<<"\t"<<data[i].major<<"\t"<<data[i].clas<<"\t"<<data[i].sex<<"\t"<<data[i].age<<"\t"<<endl;
        }
    }
    int addData(int destination,classInf datum){
        if(destination>=size){
            cout<<"List Overflow!"<<endl;
            return 0;
        }
        else{
            if(data[size-1].NO!=0){
                cout<<"List already filled!"<<endl;
                return 0;
            }
            else {
                for (int i = size - 1; i > destination; i--) {
                    data[i] = data[i - 1];
                }
                data[destination]=datum;
                return 1;
            }
        }
    }//插入
    void writeData(fstream &in){
        in.seekp(0,ios::beg);
        for(int i=0;i<size;i++&&in.good()){
                in<<data[i].NO<<data[i].name<<data[i].faculty<<data[i].major<<data[i].clas<<data[i].sex<<data[i].age<<endl;
        }
    }
};
int getData(classInf &a){
    cout<<"请输入学生学号：";
    cin>>a.NO;
    cout<<"请输入学生姓名：";
    cin>>a.name;
    cout<<"请输入学生所在学院：";
    cin>>a.faculty;
    cout<<"请输入学生专业：";
    cin>>a.major;
    cout<<"请输入学生班级：";
    cin>>a.clas;
    cout<<"请输入学生性别，0代表男性，1代表女性，2代表其他性别：";
    cin>>a.sex;
    cout<<"请输入学生年龄：";
    cin>>a.age;
}
int main() {
    cout<<"欢迎使用学生管理系统"<<endl;
    cout<<"输入选项对应序号以使用对应功能:"<<endl;
    cout<<"1.录入数据    2.插入数据"<<endl;
    cout<<"3.删除数据    4.输出所有学生信息"<<endl;
    cout<<"5.查找指定学生信息    "<<endl;
    cout<<"0.退出"<<endl;
    int i;
    cin>>i;
    int destination;
    classInf temp={0," "," "," ",0,0,0};
    //LoadFiles
    fstream in;
    in.open("Data.txt",ios::in|ios::out|ios::app);
    in.seekg(0,ios::beg);
    orderList func(in);
    //LoadFilesFinished
    switch(i){
        case 1:
            getData(temp);
            func.inputData(temp);
            break;
        case 2:
            getData(temp);
            cout<<"请输入想要插入的数据：";
            cin>>destination;
            func.addData(destination,temp);
            break;
        case 3:
            cout<<"请输入想要插入的数据：";
            cin>>destination;
            func.deleteData(destination);
            break;
        case 4:
            func.outputData();
            break;
        case 5:
            getData(temp);
            func.searchData(temp);
            break;
        default:
            func.writeData(in);
            return 0;
    }
}

