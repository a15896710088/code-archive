#include <iostream>
#include <fstream>
using namespace std;
class orderList{
    int size;
    int* data;
public:
    orderList(int size1){
        data=new int[size1];
        size=size1;
    }
    orderList(){
        data=new int[100];//默认构造大小为100的数组
        size=100;
    }
    //以上为创建环节，使用C++的构造函数
    int inputData(int destination,int datum){
        if(destination>=size){
            cout<<"List Overflow!"<<endl;
            return 0;
        }
        else{
            data[destination]=datum;
            return 1;
        }
    }//输入
    int getData(int destination){
        if(destination>=size){
            cout<<"List Overflow!"<<endl;
            return 0;
        }
        else{
            return data[destination];
        }
    }//取值
    int searchData(int datum){
        int result;
        int res=0;
        for(int i=0;i<size;i++){
            if(data[i]==datum){
                i=result;
                res=1;
                return result;
                break;
            }
        }
        if(res==0){
            cout<<"No matches!"<<endl;
            return 0;
        }
    }//查找
    int deleteData(int destination){
        if(destination>=size){
            cout<<"List Overflow!"<<endl;
            return 0;
        }
        else{
            for(int i=destination+1;i<size;i++){
                data[i-1]=data[i];
            }
            data[size-1]=0;
            return 1;
        }
    }//删除
    int addData(int destination,int datum){
        if(destination>=size){
            cout<<"List Overflow!"<<endl;
            return 0;
        }
        else{
            if(data[size-1]!=0){
                cout<<"List already filled!"<<endl;
                return 0;
            }
            else {
                for (int i = size - 1; i > destination; i--) {
                    data[i] = data[i - 1];
                }
                data[destination]=datum;
                return 1;
            }
        }
    }//插入
};
int main() {
    cout<<"请输入线性表长度："<<endl;
    int size=0;
    cin>>size;
    orderList *list=new orderList(size);
    cout<<"Please input the number before the choice you want to use:"<<endl;
    cout<<"1.input\t\t2.getdata\t\t"<<endl;
    cout<<"3.search\t\t4,delete\t\t"<<endl;
    cout<<"5.add\t\t6.exit"<<endl;
    int choice=0;
    cin>>choice;
    int destination=0;
    int data=0;
    switch(choice){
        case 1:
            cout<<"Please input the destination you want to input:"<<endl;
            cin>>destination;
            cout<<"Plaese input the datum you want to input:"<<endl;
            cin>>data;
            list->inputData(destination,data);
            break;
        case 2:
            cout<<"Please input the destination you want to get:"<<endl;
            cin>>destination;
            list->getData(destination);
            break;
        case 3:
            cout<<"Plaese input the datum you want to search:"<<endl;
            cin>>data;
            list->searchData(data);
            break;
        case 4:
            cout<<"Please input the destination you want to delete:"<<endl;
            cin>>destination;
            list->deleteData(destination);
            break;
        case 5:
            cout<<"Please input the destination you want to add:"<<endl;
            cin>>destination;
            cout<<"Plaese input the datum you want to add:"<<endl;
            cin>>data;
            list->addData(destination,data);
            break;
        case 6:
            break;
    }
    return 0;
}
