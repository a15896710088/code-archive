//
// Created by misaka13092 on 23-10-12.
//
#include <iostream>
#include <fstream>
using namespace std;
typedef int status;
typedef int datatype;
typedef struct ListNode{
    struct ListNode *next;
    datatype data;
}ListNode,*List;
int n;
status initList(List &L)
{
    L=new ListNode;
    L->next=NULL;
    return 0;
}
status createList(List &L,int n){
    List s,r;
    r=L;
    cout<<"请输入链表："<<endl;
    for(int i=0;i<n;i++){
        s=new ListNode;
        cin>>s->data;
        s->next=NULL;
        r->next=s;
        r=s;
    }
    return 0;
}
status insertList(List &L,int i,datatype x){
    List s,p;
    p=L;
    int j=0;
    while(p&&j<i-1){
        p=p->next;
        ++j;
    }
    if(!p||j>i-1)
        return 0;
    s=new ListNode;
    s->next=NULL;
    s->data=x;
    s->next=p->next;
    p->next=s;
    return 0;
}
status deleteList(List &L,int i){
    List p,q;
    p=L;
    int j=0;
    while((p->next)&&(j<i-1)){
        p=p->next;
        ++j;
    }
    if(!(p->next)&&(j>i-1))
        return 0;
    q=p->next;
    p->next=q->next;
    delete q;
    return 0;
}
status OutPutList(List L){
    List p;
    p=L->next;
    while(p!=NULL){
        cout<<p->data<<" ";
        p=p->next;
    }
    return 0;
}
int main(){
    List L;
    cout<<"1.建立"<<endl;
    cout<<"2.输入"<<endl;
    cout<<"3.插入"<<endl;
    cout<<"4.删除"<<endl;
    cout<<"5.输出"<<endl;
    cout<<"0.退出\n"<<endl;
    int choose=-1;
    while(choose!=0){
        cout<<"请选择：";
        cin>>choose;
        switch(choose){
            case 1:
                if(initList(L))
                    cout<<"建成一个空链表成功！";
                break;
            case 2:
                int n;
                cout<<"请输入单链表长度：";
                cin>>n;
                createList(L,n);
                cout<<"创建单链表完成！"<<endl;
                break;
            case 3:
                int i,x;
                cout<<"请输入插入位置和插入值：";
                cin>>i;
                cin>>x;
                if(insertList(L,i,x))
                    cout<<"插入成功！"<<endl;
                break;
            case 4:
                int j;
                cout<<"请输入删除位置：";
                cin>>j;
                if(deleteList(L,j))
                    cout<<"删除成功！"<<endl;
                break;
            case 5:
                cout<<"输出单链表为：";
                OutPutList(L);
                cout<<endl;
                break;
        }
        system("pause");
        return 0;
    }
}