cmake_minimum_required(VERSION 3.25)
project(DataExam3)

set(CMAKE_CXX_STANDARD 17)

add_executable(DataExam3
        KMP.cpp)
