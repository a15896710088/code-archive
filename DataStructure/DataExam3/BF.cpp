#include <iostream>
#include <vector>
#include <fstream>

using namespace std;
int BF(string human,string bacteria){
    int flag=0;
    int stamp=1;
    for(int i=0;i<human.length();i++){
        for(int j=0;j<bacteria.size();j++){
            if(human[i+j]!=bacteria[j]){
                stamp=0;
                break;
            }
        }
        if(stamp==1){
            flag=1;
            break;
        }
        else stamp=1;
    }
    return flag;
}
int main(){
    ifstream file;
    ofstream output;
    string human;
    vector<string> bacteria;
    string temp1;
    string temp2;
    file.open("file.txt");
    output.open("output.txt",ios::out|ios::trunc);
    file.seekg(ios::beg);
    file>>human;
    while(file.good()){
        file>>temp1;
        bacteria.push_back(temp1);
    }
    for(int i=0;i<bacteria.size();i++){
        output<<BF(human,bacteria[i])<<endl;
    }
}