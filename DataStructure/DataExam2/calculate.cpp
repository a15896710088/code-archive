#include<iostream>
using namespace std;
const char oper[7] = { '+', '-', '*', '/', '(', ')', '#' };
#define OK 1
#define ERROR 0
#define OVERFLOW (-2)
typedef char SElemType;
typedef int Status;
typedef struct SNode {
    int data;
    struct SNode *next;
} SNode, *LinkStack;

Status InitStack(LinkStack &S) {
    S = NULL;
    return OK;
}//初始化
bool StackEmpty(LinkStack S) {
    if (!S)
        return true;
    return false;
}//检查是否为空
Status Push(LinkStack &S, SElemType e) {
    SNode *p = new SNode;
    if (!p) {
        return OVERFLOW;
    }
    p->data = e;
    p->next = S;
    S = p;
    return OK;
}//顶部添加元素
Status Pop(LinkStack &S, SElemType &e) {
    SNode *p;
    if (!S)
        return ERROR;
    e = S->data;
    p = S;
    S = S->next;
    delete p;
    return OK;
}//弹出元素
Status GetTop(LinkStack &S) {
    if (!S)
        return ERROR;

    return S->data;
}//获取顶部元素
bool In(char ch) {
    for (int i = 0; i < 7; i++) {
        if (ch == oper[i]) {
            return true;
        }
    }
    return false;
}//判断是否为算子
char Precede(char theta1, char theta2) {
    if ((theta1 == '(' && theta2 == ')') || (theta1 == '#' && theta2 == '#')) {
        return '=';
    } else if (theta1 == '(' || theta1 == '#' || theta2 == '(' || (theta1== '+' || theta1 == '-') && (theta2 == '*' || theta2 == '/')) {
        return '<';
    } else
        return '>';
}//判断优先级
char Operate(char first, char theta, char second) {
    switch (theta) {
        case '+':
            return (first - '0') + (second - '0') + 48;
        case '-':
            return (first - '0') - (second - '0') + 48;
        case '*':
            return (first - '0') * (second - '0') + 48;
        case '/':
            return (first - '0') / (second - '0') + 48;
    }
    return 0;
}//操作

char EvaluateExpression() {
    LinkStack OPTR, OPND;
    char ch, theta, a, b, x, top;
    InitStack(OPND);
    InitStack(OPTR);
    Push(OPTR, '#');
    cin >> ch;
    while (ch != '#' || (GetTop(OPTR) != '#'))
    {
        if(!In(ch)){
            Push(OPND,ch);
            cin>>ch;
        }
        else{
            switch(Precede(GetTop(OPTR),ch)){
                case '<':
                    Push(OPTR,ch);
                    cin>>ch;
                    break;
                case '>':
                    Pop(OPTR,theta);
                    Pop(OPND,b);
                    Pop(OPND,a);
                    Push(OPND, Operate(a,theta,b));
                    break;
                case '=':
                    Pop(OPTR,x);
                    cin>>ch;
                    break;
            }
        }
    }
    return GetTop(OPND);
}
int menu() {
    int c;
    cout << "0-9以内的多项式计算" << endl;
    cout << "1.计算" << endl;
    cout << "0.退出\n" << endl;
    cout << "选择：";
    cin >> c;
    return c;
}
int main() {
    while (1) {
        switch (menu()) {
            case 1: {
                cout << "请输入要计算的表达式（操作数和结果都在0-9的范围内，以#结束）：" << endl;
                char res = EvaluateExpression();
                cout << "计算结果为" << res - 48 << endl << endl;
            }
                break;
            case 0:
                cout << "退出成功\n" << endl;
                exit(0);
            default:
                break;
        }
    }
}

