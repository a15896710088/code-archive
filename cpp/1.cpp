#include <iostream>
#include <vector>
using namespace std;
char compare(vector<int> v, int a[])
{
    int sum1 = 0;
    int sum2 = 0;
    for (int i = a[0]; i < a[1]; i++)
    {
        sum1 += v[i];
    }
    for (int i = a[1]; i < a[2]; i++)
    {
        sum2 += v[i];
    }
    char res = '0';
    if (sum1 == sum2)
        res = '=';
    else
        sum1 > sum2 ? res = '>' : res = '<';
    return res;
}
int main()
{
    int n, m;
    cin >> n;
    vector<int> v;
    int temp = 0;
    for (int i = 0; i < n; i++)
    {
        cin >> temp;
        v.push_back(temp);
    }
    cin >> m;
    vector<int[3]> v2;
    int temp2[3];
    for (int i = 0; i < m; i++)
    {
        cin >> temp2[0] >> temp2[1] >> temp2[2];
        v2.push_back(temp2);
    }
    char res = '0';
    for (int i = 0; i < m; i++)
    {
        res = compare(v, v2[i]);
        cout << res << endl;
    }
}