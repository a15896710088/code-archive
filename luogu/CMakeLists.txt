cmake_minimum_required(VERSION 3.26)
project(luogu)

set(CMAKE_CXX_STANDARD 14)

add_executable(luogu
        P3817.cpp)
