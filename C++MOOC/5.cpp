#include <iostream>
#include <string>
#include <cstdio>
#include <cstring>
#include <sstream>
#include <cstdlib>
using namespace std;

class Student
{
    // 在此处补充你的代码
private:
    char name[20];
    int age;
    int NO;
    static const int Year = 4;
    int score[4];
    double avg = 0;

public:
    void input()
    {
        char delimiter;
        cin.getline(name, 20, ',');
        cin >> age >> delimiter;
        cin >> NO;
        for (int i = 0; i < Year; i++)
            cin >> delimiter >> score[i];
    }
    void calculate()
    {
        int sum = 0;
        for (int i = 0; i < Year; i++)
        {
            sum += score[i];
        }
        avg = (double)sum / 4;
    }
    void output()
    {
        cout << name << ',' << age << ',' << NO << ',' << avg;
    }
};

int main()
{
    Student student;     // 定义类的对象
    student.input();     // 输入数据
    student.calculate(); // 计算平均成绩
    student.output();    // 输出数据
}